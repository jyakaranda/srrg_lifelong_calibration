close all
clear
clc

function angle = normalizeAngle(theta)
  angle = atan2(sin(theta), cos(theta));
endfunction

function traj = bernoulliLemniscate(start, dt, tf)
  x_0 = start(1);
  y_0 = start(2);
  theta_0 = start(3);
  for ts=0:dt:tf
    x = x_0 + cos(ts) / (sin(ts)*sin(ts) +1);
    y = y_0 + cos(ts)*sin(ts) / (sin(ts)*sin(ts) +1);
    theta = normalizeAngle(theta_0 + .1*sin(ts));    
    traj(end+1,:) = [x, y, theta];
  endfor
endfunction

function traj = sinusoidal(start, dt, tf)
  x_0 = start(1);
  y_0 = start(2);
  theta_0 = start(3);
  for ts=0:dt:tf
    x = x_0 + sin(start(end,3)*sin(ts));
    y = y_0 + ts;
    theta = normalizeAngle(theta_0 + sin(ts));
    traj(end+1,:) = [x, y, theta];
  endfor
endfunction

function traj = straight(start, dt, tf)
  x_0 = start(1);
  y_0 = start(2);
  theta_0 = start(3);
  for ts=0:dt:tf
    x_0 += 0.02;
    y_0 += 0.02;
    traj(end+1,:) = [x_0, y_0, theta_0];
  endfor
endfunction


                          # generate a Bernoulli Lemniscate Trajectory
dt = 0.05;
tf = 10;

trajectory = bernoulliLemniscate([0,0,0], dt, tf);
traj1 = sinusoidal(trajectory(end,:), dt, tf);
traj2 = straight(traj1(end,:), dt, tf);
trajectory = [trajectory ; traj1; traj2];

plot(trajectory(:,1), trajectory(:,2), "linewidth", 2);
hold on;

