add_library(srrg_l2c_utils_library STATIC
  utils.cpp
  file_reader.cpp
  trajectory_analyzer.cpp
  trajectory_partitioner.cpp
  trajectory_splitter.cpp
  change_detector.cpp
  )

target_link_libraries(srrg_l2c_utils_library
  srrg_l2c_types_library
  ${catkin_LIBRARIES}
)
