#pragma once
#include <srrg_l2c_types/defs.h>

namespace srrg_l2c {
  
  void isoResize(Isometry2& out2_, const Isometry2& iso2_);
  void isoResize(Isometry3& out3_, const Isometry3& iso3_);
  void isoResize(Isometry2& out2_, const Isometry3& iso3_);
  void isoResize(Isometry3& out3_, const Isometry2& iso2_);

  real computeAngle(const Matrix3& R_); 
  bool checkDistance(const Isometry3& relative_transform_,
                     const real& translation_thresh_,
                     const real& rotation_thresh_);

  std::string mangleStrings(const std::string& s1,
                            const std::string& s2);
  bool unMangleStrings(std::string& s1,
                       std::string& s2,
                       const std::string& mangled);

  Isometry3 interpolate(const double& relative_time_,
                        const Isometry3& zero_time_isometry_,
                        const Isometry3& oone_time_isometry_);

  VectorX interpolate(const double& relative_time_,
                      const VectorX& zero_time_encoder_ticks_,
                      const VectorX& oone_time_encoder_ticks_);
  
}
