#include "measure.h"

namespace srrg_l2c {

  Measure::Measure() {
    kd_tree_ptr_ = nullptr;
    status_ = Status::Unclassified;
    type_ = Type::Absolute;
  }
  
  Measure::Measure(const Isometry3& isometry_measure,
                   const Type& type) {
    Measure();
    type_ = type;
    has_ticks_ = false;
    has_depth_ = false;
    has_rgb_   = false;
    isometry_ = isometry_measure;
    encoder_ticks_.setZero(1);
  }
  
  Measure::Measure(const VectorX& encoder_ticks,
                   const Type& type) {
    Measure();
    type_ = type;
    has_ticks_ = true;
    encoder_ticks_ = encoder_ticks;      
    isometry_.setIdentity();
  }

  void Measure::setDepthImage(const FloatImage& depth_image) {
    depth_image_ = depth_image;
    has_depth_ = true;
  }
  
  void Measure::setRgbImage(const cv::Mat& rgb_image) {
    rgb_image_ = rgb_image;
    has_rgb_ = true;
  }


  const VectorX& Measure::encoderTicks() const {
    if(!has_ticks_)
      throw std::runtime_error("[Measure][encoderTicks] this measure has no encoder ticks");
    return encoder_ticks_;
  }

  const Isometry3& Measure::isometry() const {
    if(has_ticks_)
      throw std::runtime_error("[Measure][isometry] this measure has no isometry");
    return isometry_;
  }

  const bool Measure::hasTicks() const {
    return has_ticks_;
  }
  
  void Measure::transformInPlace(const Isometry3& T) {
    if(has_ticks_)
      throw std::runtime_error("[Measure][transformInPlace] this measure has no isometry");
    isometry_ = isometry_ * T;
  }
  
  Measure Measure::inFrame(const MeasurePtr& reference) {
    if(!reference)
      throw std::runtime_error("[Measure][inFrame]: nullptr");

    if(type_ == Type::Relative)
      return Measure(*this);
    
    if(has_ticks_) {
      VectorX relative_ticks = encoder_ticks_ - reference->encoderTicks();      
      for(size_t i = 0; i < relative_ticks.size(); ++i) {
        srrg_core::normalizeAngle(relative_ticks(i));
      }
      return Measure(relative_ticks);
    } else {
      Isometry3 relative = reference->isometry().inverse() * isometry_;
      return Measure(relative);
    }      
  }

  void Measure::computeTree(const real& leaf_range_) {
    if(!kd_tree_ptr_)
      kd_tree_ptr_ = KDTreePtr(new KDTree(points_, leaf_range_));
    else
      std::cerr << FGRN("[Measure][computeTree]: KD-Tree already initialized!");
  }

  KDTree::VectorTDVector& Measure::points() {
    return points_;
  }

  const KDTree::VectorTDVector& Measure::points() const {
    return points_;
  }

  Measure::Status& Measure::status(){
    return status_;
  }

  const Measure::Status& Measure::status() const {
    return status_;
  }

  const Measure::Type& Measure::type() const {
    return type_;
  }

  const FloatImage& Measure::depthImage() const {
    return depth_image_;
  }

  const cv::Mat& Measure::rgbImage() const {
    return rgb_image_;
  }
  
  const void Measure::print() const {
    if(has_ticks_)
      std::cerr << encoder_ticks_.transpose() << std::endl;
    else
      std::cerr << srrg_core::t2v(isometry_).transpose() << std::endl;
  }
  
  const KDTree& Measure::kdTree() const {
    if(!kd_tree_ptr_)
      throw std::runtime_error("[Measure][kdTree]: first call the computeTree method!");
    return *kd_tree_ptr_;
  }
  
  const bool Measure::isGood() const {
    if(has_ticks_ && encoder_ticks_.norm() < 1e-3)
      return false;    
    return true;
  }

  
}
