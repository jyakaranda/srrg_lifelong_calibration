#ifndef _L2C_SENSOR_H_
#define _L2C_SENSOR_H_

#include "defs.h"
#include <iostream>
#include <memory>

namespace srrg_l2c {


  class Sensor;

  /*! shared pointer of Sensor */
  typedef std::shared_ptr<Sensor> SensorPtr;
  /*! const shared pointer of Sensor */
  typedef std::shared_ptr<Sensor const> SensorConstPtr;
  
  /*!
   * A Sensor is an instance to be calibrated. It can
   * represent both 2d and 3d sensors, simply by setting up
   * Priors where needed. Every sensor has a different name, used
   * to index them. The user can specify
   * - initial guess
   * - prior values on specific parameters
   * - time delay estimate on/off
   *
   * `string sensor_name = "my_sensor";      // define a sensor name`<br>
   * `SensorPtr my_sensor = SensorPtr( new Sensor(sensor_name));         // create the sensor`<br>
   * `my_sensor->setExtrinsics(init_guess);   // set an initial guess (Isometry3 or Vector6)`<br>
   * `my_sensor->setEstimateTimeDelay(true);  // enable time delay estimate for this sensor`<br>
   * `my_sensor->print();                     // print sensor status`
   */
  class Sensor {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    /*! Indexes of Extrinsics Parameters */
    enum PARAM {
      X = 0, Y = 1, Z = 2, QX = 3, QY = 4, QZ = 5
    };
    
    /*!
     * A Prior is used to keep fixed some parameters.
     * Every sensor owns its own priors. As an example,
     * when a parameter isn't observable (e.g. z-value of a sensor
     * mounted on a ground robot) the user can specify a prior
     * to keep its value to a specified prior_value.
     *
     * The user interacts with priors through Sensor , by invoking the method
     * sensor.addPrior(...);
     *
     */
    struct Prior {
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

      /** Prior empty C'tor */
      Prior();

      /** initializes the Prior values.
       * @param prior_value value of the prior as transformation matrix.
       * @param param index of the param.
       * @param sensor_ptr pointer to related sensor.
       * @param magnitude the higher, the stronger the prior.
       */
      void setPrior(const Isometry3& prior_value,
                    PARAM param,
                    const Isometry3& sensor_value,
                    const real& magnitude = 1e2);

      /** computes prior contributions.
       * @param e error vector.
       * @param J jacobian matrix.
       * @param Omega omega matrix.
       */    
      void computeMatrices(Vector6& e,
                           Matrix6& J,
                           Matrix6& Omega);

      void updateSensorValue(const Isometry3& sensor_value);
      
      /** prints prior info */
      const void print() const;

    private:

      /** computes the error given a perturbation.
       * @param delta_x perturbation.
       * @return error vector
       */    
      Vector6 error(const Vector6& delta_x=Vector6::Zero()) const;

    public:      
      Isometry3 value_;            // value of prior as transformation matrix
      Isometry3 inv_value_;        // inverse of value transformation matrix
      Matrix6 information_matrix_; // information matrix storing the magnitude
      Isometry3 sensor_value_;     // value of extrinsics of related sensor
      PARAM param_prior_;          // index of this prior
      IntStringMap param_names_;   // map relating index and param names

    };

    /*! shared_ptr of Prior */
    typedef std::shared_ptr<Prior> PriorPtr;
    /*! const shared_ptr of Prior */
    typedef std::shared_ptr<Prior const> PriorConstPtr;

    /*! vector of PriorPtr */
    class PriorPtrVector : public std::vector<PriorPtr,
      Eigen::aligned_allocator<PriorPtr> > {
    public:
      ~PriorPtrVector();
    };

    /** Sensor C'tor 
     * @param sensor_name defines the sensor indexing name
     */
    Sensor(const std::string& sensor_name);

    /** Sensor D'tor */
    ~Sensor();
    
    /** specifies the frames of this sensor (used in ROS callback to get tf) 
     * @param frame_id 
     * @param child_frame_id
     */
    void setFrames(const std::string& frame_id,
                   const std::string& child_frame_id);
    

    /** specifies the information of this sensor as a matrix
     * @param information_matrix 
     */
    void setInformationMatrix(const Matrix6& information_matrix);

    /** specifies the information of this sensor as a real value
     * @param information_magnitude 
     */
    void setInformation(const real& information_magnitude);   

    /** specifies the extrinsics of this sensor as isometry
     * @param init_guess
     */
    void setExtrinsics(const Isometry3& init_guess);

    /** specifies the extrinsics of this sensor as vector
     * @param init_guess
     */
    void setExtrinsics(const Vector6& init_guess);

    /** specifies the time delay of this sensor
     * @param time_delay
     */
    void setTimeDelay(const real& time_delay);

    /** enables/disables the time delay estimate for this sensor
     * @param estimate_time_delay
     */
    void setEstimateTimeDelay(const bool estimate_time_delay);

    /** set the camera matrix of the sensor
     * @param camera_matrix
     */
    void setCameraMatrix(const Matrix3& camera_matrix);
    
    /** gets extrinsics as Isometry3 */
    const Isometry3& extrinsics() const;

    /** gets extrinsics as Vector6 */
    const Vector6& extrinsicsVector() const;
    
    /** gets inverse of extrinsics as Isometry3 */
    const Isometry3& inverseExtrinsics() const;

    /** gets time delay */
    const real& timeDelay() const;

    /** gets information matrix */
    const Matrix6& informationMatrix() const;

    /** gets sensor name */
    const std::string& name() const;
    
    /** gets sensor frame_id */
    const std::string& frameId() const;

    /** gets sensor child_frame_id */
    const std::string& childFrameId() const;    

    /** gets sensor estimate_time_delay value */
    const bool estimateTimeDelay() const;

    /** gets camera matrix */
    const Matrix3& cameraMatrix() const;
    
    /** gets sensor priors vector */
    const Sensor::PriorPtrVector& priors() const;   
    
    /** adds a prior to this sensor
     * @param prior_value the value we want to keep a certain parameter at
     * @param param parameter index
     * @param magnitude the higher, the stronger the prior
     */
    void addPrior(const real& prior_value,
                  PARAM param,
                  const real& magnitude = 1e2);

    /** updates the sensor extrinsics and time delay
     * @param dx isometry update (solution of optimization problem)
     * @param dx_td time delay update (solution of optimization problem)
     */
    void update(const Isometry3& dx,
                const real& dx_td = 0.0);   

    /** prints sensor info */
    const void print() const;

  protected:
    void updatePriorsExtrinsics() const;

  private:
    Isometry3 extrinsics_;         // sensor extrinsics as Isometry3
    Vector6 extrinsics_vector_;    // sensor extrinsics as Vector6
    Isometry3 inv_extrinsics_;     // inverse sensor extrinsics as Isometry3
    Matrix6 information_matrix_;   // sensor information matrix
    PriorPtrVector priors_;        // priors vector
    std::string sensor_name_;      // sensor name
    std::string frame_id_;         // sensor frame id
    std::string child_frame_id_;   // sensor child frame id
    real time_delay_;              // sensor time delay
    bool estimate_time_delay_;     // enabled/disabled sensor time delay
    Matrix3 camera_matrix_;        // camera matrix for camera sensors
  };
  
  
}

#endif
