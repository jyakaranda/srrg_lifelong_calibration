#include "dataset.h"

namespace srrg_l2c {

  const MeasurePtr& Dataset::nearest(const double& timestamp) const {
    Dataset::const_iterator low = this->lower_bound(timestamp);
    if (low == this->end() || low == this->begin()) {
      return low->second;
    } else {
      Dataset::const_iterator prev = std::prev(low);
      if ((timestamp - prev->first) < (low->first - timestamp))
        return prev->second;
      else
        return low->second;
    }
  }
  
  MeasurePtr Dataset::interpolateMeasure(const double& timestamp) {
    Dataset::const_iterator low = this->lower_bound(timestamp);
    const double one_time = low->first;
    if (low == this->end() || low == this->begin()) {
      return nullptr;
    } else {
      Dataset::const_iterator prev = std::prev(low);
      if(prev == this->begin())
        return nullptr;
      
      const double zero_time = prev->first;
      const double relative_time = 1.0 - (timestamp - zero_time);
      if(low->second->type() == Measure::Relative) {
        if((timestamp - zero_time) < (one_time - timestamp))
          return prev->second;
        else
          return low->second;
      }
      if(low->second->hasTicks())
        return MeasurePtr(new Measure(interpolate(relative_time,
                                                  prev->second->encoderTicks(),
                                                  low->second->encoderTicks())));
      else
        return MeasurePtr(new Measure(interpolate(relative_time,
                                                  prev->second->isometry(),
                                                  low->second->isometry())));        
    }      
  }
  
  void Dataset::transformInPlace(const Isometry3& T) {
    for(const Sample& time_measure : *this) {
      time_measure.second->transformInPlace(T);
    }
  }

  Measure::Type& Dataset::type() {
    return type_;
  }

  const Measure::Type& Dataset::type() const {
    return type_;
  }

  void Dataset::insert(const Sample& sample) {
    if(sample.second->type() != type_)
      throw std::runtime_error("[Dataset][insert]: trying to insert Sample of another type!");
    std::map<double, MeasurePtr>::insert(sample);
  }
  
  const void Dataset::write(const std::string& filename) const {
    std::cerr << FBLU("opening: ") << filename << std::endl;
    std::ofstream out_file (filename, std::ofstream::out);    
    if(!out_file) {
      std::cerr << FRED("[TimestampMeasurePtrMap][write]: unable to open file!") << std::endl;
    }
    for(Dataset::const_iterator map_it = this->begin();
        map_it != this->end();
        ++map_it ) {
      out_file << std::setprecision(20) << map_it->first << " "
               << std::setprecision(10)
               << srrg_core::t2w(map_it->second->isometry()).transpose()
               << std::endl;
    }
    out_file.close();
  }
  
  const void Dataset::write12(const std::string& filename) const {
    std::cerr << FBLU("opening: ") << filename << std::endl;
    std::ofstream out_file (filename, std::ofstream::out);    
    if(!out_file) {
      std::cerr << FRED("[TimestampMeasurePtrMap][write]: unable to open file!") << std::endl;
    }
    for(Dataset::const_iterator map_it = this->begin();
        map_it != this->end();
        ++map_it ) {
      const Isometry3& T = map_it->second->isometry();
      const Matrix3 R = T.linear();
      const Vector3 t = T.translation();

      out_file << R(0,0) << " " << R(0,1) << " " << R(0,2) << " " << t(0) << " "
               << R(1,0) << " " << R(1,1) << " " << R(1,2) << " " << t(1) << " "
               << R(2,0) << " " << R(2,1) << " " << R(2,2) << " " << t(2)
               << std::endl;
    }
    out_file.close();
  }

  const void Dataset::print() const {
    std::cerr << FGRN("[TimestampMeasurePtrMap][print] size: ") << this->size() << std::endl;
    for(const Sample& pair : *this) {
      std::cerr << FGRN("time: ") << pair.first << FGRN(" meas ");
      pair.second->print();
    }  
  }


}
