#ifndef _DATASET_H_
#define _DATASET_H_

#include "measure.h"

namespace srrg_l2c {

  /*!
   * a Dataset stores pairs of timestamp and measures.
   * it can be a relative or absolute dataset, depending on the
   * user specifications.
   */
  class Dataset : public std::map<double, MeasurePtr> {
  public:

    /** returns the closest measure ptr to specified timestamp 
     * @param timestamp required timestamp
     * @return pointer to measure
     */
    const MeasurePtr& nearest(const double& timestamp) const;
    
    /** returns the interpolated measure ptr to specified timestamp 
     * @param timestamp required timestamp
     * @return pointer to measure
     */
    MeasurePtr interpolateMeasure(const double& timestamp);

    /** transforms the dataset given an isometry
     * @param T isometry (transform to apply)
     */
    void transformInPlace(const Isometry3& T);

    /** inserts a sample in the dataset
     * @param sample the new sample
     */
    void insert(const Sample& sample);

    /** gets the dataset type */
    Measure::Type& type();

    /** gets the dataset type */
    const Measure::Type& type() const;    

    /** writes the dataset to file in format [x y z qx qy qz qw]
     * @param filename output file name
     */
    const void write(const std::string& filename) const;

    /** writes the dataset to file in Kitti format (first 3 rows of Isometry)
     * @param filename output file name
     */
    const void write12(const std::string& filename) const;
    
    /** prints the dataset */
    const void print() const;
    
  private:
    Measure::Type type_ = Measure::Type::Absolute;   // dataset type {Absolute / Relative}
  };

  /** shared pointer to Dataset */
  typedef std::shared_ptr<Dataset> DatasetPtr;  
  /** pair of string (should contain sensor name) and Dataset Ptr  */
  typedef std::pair<std::string, DatasetPtr > SensorDatasetPair;
  /** map connecting sensor names and related dataset pointers  */
  typedef std::map<std::string, DatasetPtr > SensorDatasetMap;
  /** shared pointer of SensorDatasetMap*/
  typedef std::shared_ptr<SensorDatasetMap> SensorDatasetMapPtr;
  /** const shared pointer of SensorDatasetMap*/
  typedef std::shared_ptr<SensorDatasetMap const > SensorDatasetMapConstPtr;

}

#endif
