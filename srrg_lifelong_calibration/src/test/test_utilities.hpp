#pragma once
#include <iostream>

#include <srrg_l2c_solvers/solver.h>

using namespace srrg_l2c;
using namespace srrg_core;

/**
 * generates a bernoulli lemniscate trajectory.
 * @param dataset pointer, to fill with the compute trajectory.
 * @param dt, sample rate.
 * @param tf, final time.
 */
void bernoulliLemniscate(DatasetPtr dataset,
                         const float& dt,
                         const float& tf) {
  for(float ts = 0.f; ts < tf; ts +=dt){
    const float x = cos(ts) / (sin(ts)*sin(ts) +1);
    const float y = cos(ts)*sin(ts) / (sin(ts)*sin(ts) +1);
    const float z = sin(ts)*cos(ts);
    const float qx = .1*cos(ts);
    const float qy = -.1*sin(ts);
    const float qz = .1*sin(ts);
    Vector6 v;
    v << x, y, z, qx, qy, qz;
    Eigen::Isometry3f curr_T = srrg_core::v2t(v);
    MeasurePtr measure = MeasurePtr(new Measure(curr_T));
    dataset->insert(Sample(ts, measure));
  }  
}


/**
 * applies a little noise to an isometry
 * @param the isometry.
 */
void applySomeNoise(Eigen::Isometry3f& T) {
  const float max_noise = 0.0005;
  T.translation().x() += static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max_noise));
  T.translation().y() += static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max_noise));
  T.translation().z() += static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max_noise));
  Eigen::Matrix3f m;
  m = Eigen::AngleAxisf(static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max_noise)), Eigen::Vector3f::UnitX())
    * Eigen::AngleAxisf(static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max_noise)), Eigen::Vector3f::UnitY())
    * Eigen::AngleAxisf(static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max_noise)), Eigen::Vector3f::UnitZ());
  T.linear() *= m;
}

/**
 * generates a bunch of random world points to generate a cloud
 * @param the cloud to fill.
 */
void worldPoints(srrg_core::Vector3fVector& world_points) {
  const int number_of_points = 300;
  const float max_dist = 2.0;
  world_points.resize(number_of_points, Eigen::Vector3f::Zero());
  for(size_t i = 0; i < number_of_points; ++i) {   
    world_points[i].x() += static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max_dist));
    world_points[i].y() += static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max_dist));
    world_points[i].z() += static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max_dist));
  }  
}

/**
 * computes the measurement of a cloud from a certain pose
 * @param measurements.
 * @param the cloud.
 * @param reference pose.
 */
void getRelativeMeasures(srrg_core::KDTree<real, 3>::VectorTDVector& measured_points,
                         const srrg_core::Vector3fVector& world_points,
                         const Eigen::Isometry3f& iso) {
  measured_points.resize(world_points.size());
  int it = 0;
  Eigen::Isometry3f iso_inv = iso.inverse();
  for(const Eigen::Vector3f& wp : world_points) {
    measured_points[it++] = iso_inv * wp;
  }
}
