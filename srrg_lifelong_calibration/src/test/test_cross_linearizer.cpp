#include <iostream>

#include <srrg_l2c_solvers/solver.h>
#include "test_utilities.hpp"

using namespace srrg_l2c;
using namespace srrg_core;


int main(int argv, char** argc){

  // bdc, in this test we generate dataset(s) coming from
  // multiple sensors, to test first the cross-linearizer (ICP)
  // and then the full solver

  // bdc, we call our first sensor "First-Fake-Sensor"
  std::string sensor1_name = "First-Fake-Sensor";
  // bdc, first sensor actual pose will be this one
  // x y z qx qy qz
  Vector6 sensor1_pose;
  sensor1_pose << 0.01, 0.04, 0.01, -0.25, 0.18, -0.68;
  Isometry3 sensor1_T = srrg_core::v2t(sensor1_pose);
  // bdc our first sensor has a certain delay (in sec.)
  float sensor1_time_delay = 0.0f;
  
  // bdc, we call our second sensor "Second-Fake-Sensor"
  std::string sensor2_name = "Second-Fake-Sensor";
  // bdc, second sensor actual pose will be this one
  // x y z qx qy qz
  Vector6 sensor2_pose;
  sensor2_pose << 0.03, 0.05, 0.01, -0.27, 0.15, -0.66;
  Isometry3 sensor2_T = srrg_core::v2t(sensor2_pose);
  // bdc our second sensor has a certain delay (in sec.)
  float sensor2_time_delay = 0.0f;

  // bdc, now our dataset
  const int data_size = 5000;
  // bdc, dataset sample rate
  float dt = 0.005;
  // bdc, let's generate every time something new
  srand (static_cast <unsigned> (time(NULL)));

  // bdc, we need an odometry dataset
  // i.e. the trajectory of our platform
  DatasetPtr odometry_dataset = DatasetPtr(new Dataset());
  // let's use a 3d bernoulli lemniscate (an eight-shape)
  bernoulliLemniscate(odometry_dataset, dt, 20);
  // bdc, a dataset for the our First Sensor
  DatasetPtr sensor1_dataset = DatasetPtr(new Dataset());
  // bdc, a dataset for the our Second Sensor
  DatasetPtr sensor2_dataset = DatasetPtr(new Dataset());

  // bdc, we have a world made of points to get the sensor measurements
  srrg_core::Vector3fVector world_points;
  worldPoints(world_points);
  
  // bdc, populate the sensor datasets
  for(const Sample& pair_time_meas : *odometry_dataset) {
    // bdc, the current timestamp
    const float ts = pair_time_meas.first;

    Isometry3 sensor1_iso = pair_time_meas.second->isometry() * sensor1_T;
    // bdc, apply some noise to the measurement
    applySomeNoise(sensor1_iso);
    MeasurePtr sensor1_measure_ptr(new Measure(sensor1_iso));
    // bdc, get the relative measure (as points) from the sensor1 pose
    getRelativeMeasures(sensor1_measure_ptr->points(), world_points, sensor1_iso);
    // each measurement has a KD tree to initialize with the measurements gathered
    sensor1_measure_ptr->computeTree();
    // bdc, when inserting the sample in the dataset, we put a delayed timestamp
    sensor1_dataset->insert(Sample(ts-sensor1_time_delay,sensor1_measure_ptr));

    Isometry3 sensor2_iso = pair_time_meas.second->isometry() * sensor2_T;
    applySomeNoise(sensor2_iso);
    MeasurePtr sensor2_measure_ptr(new Measure(sensor2_iso));
    // bdc, get the relative measure (as points) from the sensor1 pose
    getRelativeMeasures(sensor2_measure_ptr->points(), world_points, sensor2_iso);
    // each measurement has a KD tree to initialize with the measurements gathered
    sensor2_measure_ptr->computeTree();
    // bdc, when inserting the sample in the dataset, we put a delayed timestamp
    sensor2_dataset->insert(Sample(ts-sensor2_time_delay,sensor2_measure_ptr));
  }

  // bdc, now insert the datasets in a single map
  SensorDatasetMap sensor_dataset_map;
  sensor_dataset_map.insert(SensorDatasetPair(sensor1_name, sensor1_dataset));
  sensor_dataset_map.insert(SensorDatasetPair(sensor2_name, sensor2_dataset));
  
  std::cerr << "[Random Dataset Generated]!" << std::endl;
 
  // bdc, initialize a sensor with the same name used in the respective dataset
  SensorPtr sensor1 = SensorPtr(new Sensor(sensor1_name));
  // bdc, initialize our sensor with an identity as extrinsics
  sensor1->setExtrinsics(Vector6::Zero());

  // bdc, let's do similar stuff for the secondo sensor
  SensorPtr sensor2 = SensorPtr(new Sensor(sensor2_name));  
  sensor2->setExtrinsics(Vector6::Zero());

  // bdc, print out our initial state
  std::cerr << "Initial State" << std::endl;
  sensor1->print();
  sensor2->print();

  // bdc, as first TEST, try the cross linearize alone  
  CrossLinearizer cross_linearizer;
  cross_linearizer.setVerbosity(true);
  // bdc, in this linearizer one sensor acts as reference, the other as current
  cross_linearizer.setSensor(sensor2);
  cross_linearizer.setReferenceSensor(sensor1);
  cross_linearizer.init();

  cross_linearizer.setReferenceDataset(sensor1_dataset);
  cross_linearizer.setSensorDataset(sensor2_dataset);
  
  // bdc, hand-made least squares with the cross-linearizer H and b
  const int iterations = 10;
  MatrixX H = MatrixX::Zero(12,12);
  VectorX b = VectorX::Zero(12);  
  for(size_t i = 0; i < iterations; ++i) {
    real chi = 0;
    int outliers = 0;
    int inliers = 0;
    H.setZero();
    b.setZero();    
    cross_linearizer.linearize(H, b, chi, inliers, outliers);
    VectorX dx = H.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(-b);
    sensor1->update(srrg_core::v2tEuler(Vector6(dx.head(6))));
    sensor2->update(srrg_core::v2tEuler(Vector6(dx.tail(6))));
    std::cerr << "inliers: " << inliers << " outliers: " << outliers << " chi: " << chi << std::endl;    
  }
  
  // bdc, print out the difference between the relative transformation got and the one expected
  std::cerr << FRED("Looking for relative transform with Cross-Linearizer alone") << std::endl;
  std::cerr << FGRN("Found CL: ") << srrg_core::t2v(sensor1->extrinsics().inverse() * sensor2->extrinsics()).transpose() << std::endl;
  std::cerr << FGRN("Desired : ") << srrg_core::t2v(sensor1_T.inverse() * sensor2_T).transpose() << std::endl;

  // bdc, now use the proper tool, i.e. the solver
  std::cerr << std::endl << FRED("Using full Solver") << std::endl;  

  // test solver with exact same data and sensors
  // bdc, reset sensor poses
  sensor1->setExtrinsics(Vector6::Zero());
  sensor2->setExtrinsics(Vector6::Zero());
  std::cerr << "Initial State" << std::endl;
  sensor1->print();
  sensor2->print();
  
  // bdc, our solver needs to know the kinematics (if any, in this case we use directly
  // the odometry, so it does not need it) and the sensors (the pointers)
  Solver solver;
  solver.setVerbosity(true); 
  solver.setSensor(sensor1);
  solver.setSensor(sensor2);
  // bdc, set also the correlation
  solver.setCorrelation(sensor1, sensor2);
  solver.setIterations(20);
  solver.setEpsilon(1e-4);
  
  // bdc, always remember to initialize the solver
  solver.init();
  
  solver.compute(odometry_dataset, sensor_dataset_map);
  solver.stats().print();
  
  sensor1->print();
  std::cerr << "Actual Solution S1  : " << sensor1_pose.transpose() << std::endl;

  sensor2->print();
  std::cerr << "Actual Solution S2  : " << sensor2_pose.transpose() << std::endl;
    
  return 0;
}
