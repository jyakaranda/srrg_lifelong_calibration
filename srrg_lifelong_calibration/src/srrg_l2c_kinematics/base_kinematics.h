#pragma once
#include <srrg_l2c_types/defs.h>
#include <srrg_types/types_mat.hpp>
#include <srrg_system_utils/colors.h>
#include <memory>

namespace srrg_l2c{
  
  /*!
   * Base Class for Robots Kinematics. This class is abstract.
   * refer to DifferentialDriveKinematics for an example of
   * base kinematics derivation. Basically the user has to specify
   * a directKinematics method and an update function
   */
  class BaseKinematics {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
   
    /** empty c'tor */
    BaseKinematics() {}
    
    /** empty d'tor */
    virtual ~BaseKinematics() {}

    /** computes direct kinematics
     * @param pose starting 2d pose [x, y, yaw]
     * @param encoder_ticks encoder counts
     */
    virtual void directKinematics(Vector3& pose,
                                  const VectorX& encoder_ticks) = 0;

    /** computes direct kinematics
     * @param pose starting 2d pose [x, y, yaw]
     * @param encoder_ticks encoder counts
     * @param odom_params redundant params overriding object attribute
     */
    virtual void directKinematics(Vector3& pose,
                                  const VectorX& encoder_ticks,
                                  const VectorX& odom_params) = 0;
    
    /** updates the kinematics parameter
     * @param dx update vector
     */
    virtual void update(const VectorX& dx) = 0;
       
    /** prints the kineamtics info */
    virtual const void print() const = 0;

    /** sets the kineamtics parameters
     * @param init_guess initial estimate of kineamtics parameters
     */
    inline void setOdomParams(const VectorX& init_guess) {
      odom_params_ = init_guess;
    }

    /** gets the kinematics params */
    inline VectorX& odomParams() {
      return odom_params_;
    }

    /** gets the kinematics params */
    inline const VectorX& odomParams() const {
      return odom_params_;
    }
    
  protected:
    VectorX odom_params_;      // kineamtics params stored as a dynamic vector
  };

  /** shared pointer to BaseKinematics */
  typedef std::shared_ptr<BaseKinematics> BaseKinematicsPtr;
  /** const shared pointer to BaseKinematics */
  typedef std::shared_ptr<BaseKinematics const> BaseKinematicsConstPtr;
  
}
