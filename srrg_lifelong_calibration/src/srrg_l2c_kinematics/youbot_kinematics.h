#pragma once
#include "base_kinematics.h"

namespace srrg_l2c{
  
  class YoubotKinematics: public BaseKinematics{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;    

    static const int intrinsics_size = 5;
    // geom_factor : i.e. baselines ratio
    // w_FL : i.e. front-left  wheel radius/speed ratio
    // w_FR : i.e. front-right wheel radius/speed ratio
    // w_BL : i.e. back-left   wheel radius/speed ratio
    // w_BR : i.e. back-right  wheel radius/speed ratio
    
    YoubotKinematics(){
      odom_params_.setZero(intrinsics_size);
    }
    
    ~YoubotKinematics(){}
    
    void directKinematics(Vector3& pose,
                          const VectorX& encoder_ticks);
    void directKinematics(Vector3& pose,
                          const VectorX& encoder_ticks,
                          const VectorX& odom_params);
    
    void update(const VectorX& dx) {
      odom_params_ += dx;
    }

    const void print() const;
 
  };

  typedef std::shared_ptr<YoubotKinematics> YoubotKinematicsPtr;
  typedef std::shared_ptr<YoubotKinematics const> YoubotKinematicsConstPtr;
 
  
}
