#pragma once

#include "solver.h"
#include <Eigen/StdVector>

namespace srrg_l2c {

  struct TimeDelay {
    std::vector<real> _delays;
    const void print() const {
      std::cerr << FMAG("delays: ");
      for(const real& val : _delays)
        std::cerr << val << " ";
      std::cerr << std::endl;
    }
  };

  struct BackupAndBest{
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    BackupAndBest(){
      _backup.setIdentity();
      _best.setIdentity();
    }
    BackupAndBest(const Isometry3& backup_) {
      _backup = backup_;
      _best = backup_;
    }
    Isometry3 _backup;
    Isometry3 _best;    
  };

  typedef std::pair<TimeDelay, Solver*> TimeDelaySolverPair;
  
  struct BestSolver{
    real chi = 1e8;
    TimeDelaySolverPair delay_solver_pair;
  };
  
  typedef std::vector<TimeDelaySolverPair, Eigen::aligned_allocator<TimeDelaySolverPair> > TimeDelaySolverPairVector;
  typedef std::vector<std::pair<BackupAndBest, Sensor*>,
    Eigen::aligned_allocator<std::pair<BackupAndBest, Sensor*> > > SensorBackupVector;
  
  class Calibrator{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    struct Config {
      real min_delay = -.5;
      real max_delay = .5;
      real delay_sampling_time = .1;      
    };
    
    Calibrator();
    ~Calibrator();

    inline const Config& config() const {return _config;}
    inline Config& mutableConfig() {return _config;}

    void setKinematics(BaseKinematics* kinematics_);
    void setSensor(Sensor* sensor_);  
    void setEpsilon(const real& epsilon_);
    void setEpsilonTime(const real& epsilon_time_);
    void setVerbosity(const bool verbose_);
    void setIterations(const int iterations_);
    
    void init();

    void compute(const TimestampMeasurePtrMap& reference_dataset_,
                 const SensorDatasetMap& sensor_dataset_);
    
  private:
    void computeTimeDelay(TimeDelay& time_delay_,
                          const int id,
                          const int num_samples,
                          const int num_dim);
    void setTimeDelay(const TimeDelay& time_delay_);
    
    void resetBackupParams();
    void storeBestParams();
    void setBestParams();
    
    Config _config;

    BaseKinematics* _kinematics;
    bool _initialized;
    bool _verbose;

    VectorX _odom_param_backup;
    VectorX _odom_param_best;
    
    SensorBackupVector _sensor_vector;

    int _iterations;
    real _epsilon;
    real _epsilon_time;

    TimeDelaySolverPairVector _solver_vector;
    
  };

  
}
