#include "calibrator_imgui.h"

namespace srrg_l2c {

  CalibratorGUI::CalibratorGUI(const Mode m_): CalibratorRos(m_) {
    _chi_stats = NULL;
    _size_of_stats = 0;
  }

  CalibratorGUI::~CalibratorGUI() {
    if(_chi_stats)
      free(_chi_stats);
  }


  void CalibratorGUI::drawRobotStatus() {
    if(!_robot)
      return;
    
    ImGui::TextColored(Colors::Green, "Kinematics Parameters");
    // todo make this more efficient
    const VectorX& odom_parameters = _robot->odomParams();
    for(size_t i=0; i < odom_parameters.size(); ++i) {
      ImGui::Text("%.3f, ", odom_parameters(i));
      ImGui::SameLine();
    }
    ImGui::Spacing();
  }


  void CalibratorGUI::drawSensorStatus() {
    for(SensorPtr sensor : _sensor_ptr_vector) {
      ImGui::TextColored(Colors::Green, sensor->name().c_str());
      const Vector6 v = sensor->extrinsicsVector();
      ImGui::Text("x: %.3f, y: %.3f, z: %.3f\nqx: %.3f, qy: %.3f, qz: %.3f", v(0), v(1), v(2), v(3), v(4), v(5));
      ImGui::Spacing();
    }

  }

  void CalibratorGUI::updateStats() {
    const Solver::Stats& solver_stats = _solver.stats();
    _size_of_stats = solver_stats.chi_evolution.size();
    if(_chi_stats)
      free(_chi_stats);
    _chi_stats = (float*)malloc(_size_of_stats * sizeof(float));
    float* head = _chi_stats;
    for(const float& chi_val : solver_stats.chi_evolution) {
      *head = chi_val;
      head++;
    }    
  }
  
  void CalibratorGUI::showGUI(bool *p_open) {
    
    ImGuiWindowFlags window_flags = 0;
    window_flags |= ImGuiWindowFlags_MenuBar;
 
    ImGui::SetNextWindowSize(ImVec2(550, 680), ImGuiCond_FirstUseEver);
    if (!ImGui::Begin("CalibratorGUI", p_open, window_flags)) {
      // Early out if the window is collapsed, as an optimization.
      ImGui::End();
      return;
    }
  
    /// MAIN WINDOW CONTENT
    ImGui::TextColored(Colors::Green, "CalibratorGUI");
    ImGui::Spacing();
    ImGui::Text("Mode: ");
    ImGui::SameLine();
    if(_mode == ONLINE)
      ImGui::TextColored(Colors::Yellow, "ONLINE");
    else
      ImGui::TextColored(Colors::Yellow, "OFFLINE");
      
    ImGui::Separator();
    ImGui::Columns(2, "yaml-bag-columns");
    {
      ImGui::Text("Parameters Filename");
      ImGui::Text("%s", _yaml_filename.c_str());
      // TODO: avoid append of sensors. -> clear and reload
      if(ImGui::Button("Re-Load"))
        std::cerr << "Yaml Reload to implement. Sorry" << std::endl;
      ImGui::NextColumn();
      ImGui::Text("Bag Filename");
      ImGui::Text("%s", _bag_file.c_str());
      // TODO: avoid append of sensors. -> clear and reload
      if(ImGui::Button("Re-Load"))
        std::cerr << "Bag Reload to implement. Sorry" << std::endl;
        //initFromYaml(_yaml_filename);      
    }
    ImGui::Columns(1);
    ImGui::Separator();

    ImGui::Columns(2, "param_status-plot");
    ImGui::TextColored(Colors::Orange, "Parameters");
    drawRobotStatus();
    drawSensorStatus();
    ImGui::NextColumn();
    // Plot Trajectory TODO
    ImGui::Columns(1);

    ImGui::Separator();
    ImGui::Spacing(); 

    ImGui::Columns(2, NULL, false);
    if(ImGui::Button("Compute")) {
      compute();
      updateStats();
    }
    ImGui::NextColumn();
    if(ImGui::Button("Save YAML")) {
      std::cerr << "not implemented yet\n";
    }
    ImGui::Columns(1);
    ImGui::Spacing();
    
    ImGui::PlotLines("Chi Evolution", _chi_stats, _size_of_stats);

    
  }
  
}
