add_subdirectory(imgui)

add_library(srrg_l2c_imgui_library SHARED
  calibrator_imgui.h calibrator_imgui.cpp  
)

target_link_libraries(srrg_l2c_imgui_library
  dear_imgui
  ${catkin_LIBRARIES}
  ${OpenCV_LIBS}
  )
