#include "dead_reckoning_viewer.h"
#include <qevent.h>

#include <srrg_gl_helpers/opengl_primitives.h>

namespace srrg_l2c {

  using namespace srrg_gl_helpers;

  void DeadReckoningViewer::drawTrajectory() {
    //draw here the trajectory as set of reference frames
    Eigen::Vector3f start_pose(Eigen::Vector3f::Zero());
    bool jump_first = true;
    int counter = 0;
    for(std::map<double, Eigen::Isometry3f>::const_iterator it = _robot_poses.begin();
        it != _robot_poses.end(); ++it, ++counter) {
      if(jump_first) {
        jump_first = false;
        continue;
      }
      const Eigen::Isometry3f& pose = it->second;
      // std::cerr << "T:\n" << pose.matrix() << std::endl;
      // std::cerr << "start_pose: " << start_pose.transpose() << std::endl;
      
      // glPushMatrix();
      // {
      //   glBegin(GL_LINES);
      //   {
      //     glColor4f(1.0, 0.0, 0.0, 1.0);
      //     glVertex3f(start_pose.x(), start_pose.y(), start_pose.z());
      //     start_pose = pose.translation();
      //     glVertex3f(start_pose.x(), start_pose.y(), start_pose.z());
      //   }
      //   glEnd();
      // }
      // glPopMatrix();
      
      if((counter%50) == 0) {
        glPushMatrix();
        {
          glMultMatrix(pose);
          glPushMatrix();
          {
            glScalef(0.3, 0.3, 0.3);
            drawReferenceSystem();
          }
          glPopMatrix();

        }
        glPopMatrix();
      }
    }
  }

  void DeadReckoningViewer::drawCloud() {
    glPushMatrix();
    glColor4f(0.0, 0.0, 1.0, .6);
    _cumulative_cloud.draw();
    glPopMatrix();
  }
  
  void DeadReckoningViewer::draw() {
    if(!_dataset_ptr)
      return;
    
    glPushMatrix();
    glLineWidth(5.0);
    drawTrajectory();
    glPopMatrix();

    glPushMatrix();
    drawCloud();
    glPopMatrix();
    
  }

  void DeadReckoningViewer::colorFromStatus(const Measure::Status& status_) {
    if(status_ == Measure::Status::StraightConstant)
      glColor4f(0.5, 0.5, 1.0, 0.8);
    else if(status_ == Measure::Status::StraightVariable)
      glColor4f(0.8, 0.8, 1.0, 0.8);
    else if(status_ == Measure::Status::ArcConstant)
      glColor4f(0.5, 1.0, 0.5, 0.8);
    else if(status_ == Measure::Status::ArcVariable)
      glColor4f(0.8, 1.0, 0.8, 0.8);
    else if(status_ == Measure::Status::RotateConstant)
      glColor4f(1.0, 0.5, 0.5, 0.8);    
    else if(status_ == Measure::Status::RotateVariable)
      glColor4f(1.0, 0.8, 0.8, 0.8);    
    else
      glColor4f(0.1, 0.1, 0.1, 0.1);
  }
  
  void DeadReckoningViewer::keyPressEvent(QKeyEvent *event) {
    VectorX& odom_param = _robot_ptr->odomParams();
    switch(event->key()) {
      
    case Qt::Key_Plus:
      _increment = true;
      break;
    case Qt::Key_Minus:
      _increment = false;
      break;
    case Qt::Key_R:
      if(_increment)
        odom_param(0) += 0.003;
      else
        odom_param(0) -= 0.003;
      break;
    case Qt::Key_L:
      if(_increment)
        odom_param(1) += 0.003;
      else
        odom_param(1) -= 0.003;
      break;
    case Qt::Key_B:
      if(_increment)
        odom_param(2) += 0.001;
      else
        odom_param(2) -= 0.001;
      break;
    case Qt::Key_C:
      std::cerr << "re-computing" << std::endl;
      compute();
      break;      
    }
    std::cerr << "robot: " << _robot_ptr->odomParams().transpose() << std::endl;
    updateGL();
  }
  
  void DeadReckoningViewer::compute() {

    if(!_dataset_ptr || !_robot_ptr)
      throw std::runtime_error("[Viewer][compute]: missing dataset and/or robot ptrs");

    std::ofstream traj_writer;
    traj_writer.open("dead_reckoning_trajectory.txt");
    
    std::cerr << "[Viewer][compute]" << std::endl;
    _robot_poses.clear();
    Eigen::Vector3f pose;
    pose.setZero();

    Eigen::VectorXf old_ticks, current_ticks;
    bool first = true;
    for(const Sample& sample : *_dataset_ptr) {
      if(first) {
        old_ticks = sample.second->encoderTicks();
        first = false;
        continue;
      }
      current_ticks = sample.second->encoderTicks();
      Eigen::VectorXf relative_ticks = current_ticks - old_ticks;
      for(size_t i = 0; i < relative_ticks.size(); ++i) {
        srrg_core::normalizeAngle(relative_ticks(i));
      }
      _robot_ptr->directKinematics(pose, relative_ticks);
      Eigen::Isometry3f poseT; poseT.setIdentity();
      poseT.translation().head(2) = pose.head(2);
      poseT.linear() = srrg_core::Rz(pose(2));

      // if(relative_ticks.norm() < 1e-3)
      //   poseT.setIdentity();
      
      std::cerr << poseT.matrix() << std::endl;

      traj_writer << std::setprecision(20) << sample.first << " "
                  << std::setprecision(10) << srrg_core::t2w(poseT).transpose() << std::endl;
      _robot_poses.insert(std::make_pair(sample.first ,poseT));
      old_ticks = current_ticks;
    }

    traj_writer.close();
    
    _cumulative_cloud.clear();
    SensorDatasetMap::const_iterator sensor_data_it = _sensor_dataset_ptr->find(_sensor_ptr->name());
    for(const Sample& sample : *sensor_data_it->second) {
      Float3Image point_image;
      point_image.create(_direction_image.rows, _direction_image.cols);
      srrg_core::computePointsImage(point_image, _direction_image, sample.second->depthImage(), 0.2, 2.0);

      cv::imshow("point_image", point_image);
      cv::waitKey(10);
      
      const double& timestamp = sample.first;
      std::map<double, Isometry3>::const_iterator low = _robot_poses.lower_bound(timestamp);
      
      
      const Isometry3& robot_T = low->second;
      const Isometry3 sensor_T = robot_T * _sensor_ptr->extrinsics();

      srrg_core::Cloud3D current_cloud;
      Eigen::Vector3f normal(1.0, 0, 0);
      current_cloud.resize(_direction_image.rows * _direction_image.cols);
      int cloud_id = 0;
      for(int r = 0; r < _direction_image.rows; ++r)
        for(int c = 0; c < _direction_image.cols; ++c) {
          const cv::Vec3f point_cv = point_image.at<cv::Vec3f>(r,c);
          const Eigen::Vector3f point_eig(point_cv[0], point_cv[1], point_cv[2]);
          if(point_eig.norm() < 1e-3)
            continue;
          current_cloud[cloud_id++] = srrg_core::RichPoint3D(point_eig, normal, 1.0);
        }
      current_cloud.resize(cloud_id);
      current_cloud.transformInPlace(sensor_T);
      _cumulative_cloud.add(current_cloud);
      _cumulative_cloud.voxelize(0.05);

    }

    std::cerr << "cumulative cloud size: " << _cumulative_cloud.size() << std::endl;
    
    std::cerr << "[Viewer][compute]: done" << std::endl;  
  }
}


