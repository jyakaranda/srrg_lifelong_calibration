# SRRG Lifelong Calibration #

Heterogeneous Multi-Sensor Calibrator featuring Time Delay estimate. Odin guides us to Valhalla.

## Preliminary Requirements

- [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules)
- [srrg_gl_helpers](https://gitlab.com/srrg-software/srrg_gl_helpers)   
- [srrg_core](https://gitlab.com/srrg-software/srrg_core)
- `sudo apt install libglfw3-dev`
- `sudo apt install libqglviewer-dev`

## Test apps

    rosrun srrg_lifelong_calibration test_time_estimate

It run a synthetic test with a 3d trajectory (__bernoulli-lemniscate__) as reference
and two 3d sensors (whose measures are noisy), estimating their extrinsics and their
time delay. The estimate starts from scratch, so Identity matrix as extrinsics and 0 second
for each sensor as delay.

    rosrun srrg_lifelong_calibration test_cross_linearizer

Runs a double synthetic test. First only Cross-constraint between two 3d sensor using a linearizer
that runs ICP on the synthetic noisy point clouds. Then it runs a full optimization, i.e.,
considering both odometry-sensor and sensor-sensor constraints.

