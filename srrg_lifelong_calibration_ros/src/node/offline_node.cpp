#include <iostream>
#include <srrg_system_utils/system_utils.h>
#include <srrg_l2c_ros/calibrator_ros.h>

using namespace srrg_l2c;

const char* banner [] = {
  "offline_node: from nw to valhalla",
  "",
  "usage: offline_node -config configuration.yaml bagfile.bag",
  "-config  <string>        yaml configuration file, see default.yaml",
  "-h       <flag>          this help",
  0
};


int main(int argc, char** argv) {
  
  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
  }

  int c = 1;
  std::string configuration_file = "";
  std::string bag_file = "";
  
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-config")) {
      c++;
      configuration_file = argv[c];
    } else {
      bag_file = argv[c];
    }
    c++;
  }

  if(configuration_file.empty())
    throw std::runtime_error("[LifelongCalibrator] specify a YAML configuration file");

  // TODO: remove ros_init dependency
  ros::init(argc, argv, "lifelong_calibration_offline_node");
  ros::NodeHandle nh("~");
  ROS_INFO("node started");
  
  CalibratorRos calibrator(CalibratorRos::OFFLINE);
  calibrator.setBagFile(bag_file);
  calibrator.initFromYaml(configuration_file);
  calibrator.compute();
  
  return 0;
}
