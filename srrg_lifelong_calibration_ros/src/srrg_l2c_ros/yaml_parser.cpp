#include "yaml_parser.h"

namespace srrg_l2c {
 
  YamlParser::YamlParser() {
    _joint_topic = "";
    _odom_topic = "";
    _robot_type = "";
    _block_size = 30;
    _iterations = 30;
    _rate = 2.0;
  }

  YamlParser::YamlParser(const std::string& yaml_file) {
    _configuration = YAML::LoadFile(yaml_file);
    YamlParser();
  }

  YamlParser::~YamlParser() {
    std::cerr << FRED("[YAMLParser] ");
    std::cerr << FRED("deleted\n");
  }

  void YamlParser::setYamlFile(const std::string &yaml_file_) {
    _configuration = YAML::LoadFile(yaml_file_);
  }
  
  const std::string& YamlParser::jointTopic() const {return _joint_topic;}

  const std::string& YamlParser::odomTopic() const {return _odom_topic;}
  
  const std::string& YamlParser::robotType() const {return _robot_type;}

  const int YamlParser::blockSize() const {return _block_size;}

  const double& YamlParser::rate() const {return _rate;}

  const int YamlParser::iterations() const {return _iterations;}
  
  const VectorX& YamlParser::robotIntrinsics() const {return _robot_intrinsics;}
  
  const void YamlParser::print() const {
    std::cerr << "joint_topic: " << _joint_topic << std::endl;
    std::cerr << "odom_topic:  " << _odom_topic << std::endl;
    std::cerr << "robot_type:  " << _robot_type << std::endl;
    if(!_robot_type.empty())
      std::cerr << "robot intrinsics: " << _robot_intrinsics.transpose() << std::endl;
    // for(const SensorPtr& sensor : _sensor_vector)
    //   sensor->print();
  }

  void YamlParser::parse(SensorPtrVector& sensor_vector) {
    if(_configuration["block_size"])
      _block_size = _configuration["block_size"].as<int>();
    if(_configuration["rate"])
      _rate = _configuration["rate"].as<double>();
    if(_configuration["iterations"])
      _iterations = _configuration["iterations"].as<int>();
    
    if(_configuration["reference_topic"]["joint_topic"])
      _joint_topic = _configuration["reference_topic"]["joint_topic"].as<std::string>();
    if(_configuration["reference_topic"]["odom_topic"])
      _odom_topic = _configuration["reference_topic"]["odom_topic"].as<std::string>();
    if(_configuration["reference_topic"]["robot_type"])
      _robot_type = _configuration["reference_topic"]["robot_type"].as<std::string>();
    if(_configuration["reference_topic"]["robot_intrinsics"]) {
      std::vector<real> robot_intrinsics = _configuration["reference_topic"]["robot_intrinsics"].as<std::vector<real> >();
      _robot_intrinsics.setZero(robot_intrinsics.size());
      for(size_t i = 0; i < robot_intrinsics.size(); ++i)
        _robot_intrinsics(i) = robot_intrinsics[i];
    }

    
    if(_configuration["Sensors"].IsSequence()) {
      for(YAML::const_iterator it = _configuration["Sensors"].begin();
          it != _configuration["Sensors"].end(); ++it) {
        if(!(*it)["name"])
          throw std::runtime_error("[YAMLParser] Each Sensor HAS to have a name!");        
        SensorPtr sensor = SensorPtr(new Sensor((*it)["name"].as<std::string>()));
        if((*it)["frame_id"] && (*it)["child_frame_id"])
          sensor->setFrames((*it)["frame_id"].as<std::string>(),
                            (*it)["child_frame_id"].as<std::string>());
        Vector6 initial_guess;
        initial_guess(0) = (*it)["initial_guess"]["x"].as<float>();        
        initial_guess(1) = (*it)["initial_guess"]["y"].as<float>();
        initial_guess(2) = (*it)["initial_guess"]["z"].as<float>();
        initial_guess(3) = (*it)["initial_guess"]["qx"].as<float>();
        initial_guess(4) = (*it)["initial_guess"]["qy"].as<float>();
        initial_guess(5) = (*it)["initial_guess"]["qz"].as<float>();
        sensor->setExtrinsics(initial_guess);

        if((*it)["estimate_time_delay"])
          sensor->setEstimateTimeDelay((*it)["estimate_time_delay"].as<bool>());
        
        if((*it)["information_magnitude"])
          sensor->setInformation((*it)["information_magnitude"].as<float>());
        
        if((*it)["has_prior"].IsSequence()){
          for(YAML::const_iterator prior_it = (*it)["has_prior"].begin();
              prior_it != (*it)["has_prior"].end(); ++prior_it) {
            std::string coordinate = (*prior_it)["coordinate"].as<std::string>();
            if(coordinate == "x")
              sensor->addPrior(initial_guess(0), Sensor::X);
            else if(coordinate == "y")
              sensor->addPrior(initial_guess(1), Sensor::Y);
            else if(coordinate == "z")
              sensor->addPrior(initial_guess(2), Sensor::Z);
            else if(coordinate == "qx")
              sensor->addPrior(initial_guess(3), Sensor::QX);
            else if(coordinate == "qy")
              sensor->addPrior(initial_guess(4), Sensor::QY);
            else if(coordinate == "qz")
              sensor->addPrior(initial_guess(5), Sensor::QZ);
          }
        }
        sensor_vector.push_back(sensor);
      }      
    } // end _Sensors sequence
    
  } // end parse()
  
} // end namespace
