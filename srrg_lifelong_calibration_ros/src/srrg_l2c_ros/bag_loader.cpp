#include "bag_loader.h"

namespace srrg_l2c {

  BagLoader::BagLoader() {
    BagLoader(std::string(""));
  }

  BagLoader::BagLoader(const std::string& bag_file) {
    _bag_file = bag_file;
    _joint_number = 0;
  }
  
  BagLoader::~BagLoader() {
  }

  void BagLoader::setBagFile(const std::string& bag_file_) {
    _bag_file = bag_file_;
  }
  
  void BagLoader::setJointTopic(const std::string& joint_topic_,
                                const int joint_number_) {
    _joint_topic = joint_topic_;
    _joint_number = joint_number_;
  }
  
  void BagLoader::setOdomTopic(const std::string& odom_topic_) {
    _odom_topic = odom_topic_;
  }

  void BagLoader::setImageTopic(const std::string& sensor_name,
                                const std::string& depth_topic,
                                const std::string& rgb_topic) {
    _image_topics = (std::make_pair(sensor_name, depth_topic));
  }
  
  void BagLoader::setFramesHandlerVector(const FramesHandlerVector& frames_handler) {
    _frames_handler_vector = frames_handler;    
  }

  
  void BagLoader::generateJointMessage(Dataset& dataset_,
                                       const sensor_msgs::JointStateConstPtr& joint_message_,
                                       const double& timestamp_) {
    VectorX encoder_ticks;
    encoder_ticks.setZero(_joint_number);
    for(size_t i = 0; i < _joint_number; ++i)
      encoder_ticks(i) = joint_message_->position[i];

    MeasurePtr measure(new Measure(encoder_ticks));
    dataset_.insert(Sample(timestamp_, measure));
  }
  
  void BagLoader::generateOdomMessage(Dataset& dataset_,
                                      const nav_msgs::OdometryConstPtr& odom_message_,
                                      const double& timestamp_) {
    Isometry3 isometry;
    nav2iso(isometry, odom_message_);
    
    MeasurePtr measure(new Measure(isometry));
    dataset_.insert(Sample(timestamp_, measure));
  }

  void BagLoader::generateTfMessage(SensorDatasetMap& sensor_dataset_,
                                    const tf2_msgs::TFMessageConstPtr& tf_message_,
                                    const double& timestamp_) {
    
    const std::string frame_id       = (*tf_message_).transforms[0].header.frame_id;
    const std::string child_frame_id = (*tf_message_).transforms[0].child_frame_id;

    // get name of sensor from frames_handler
    std::string sensor_name = "";
    
    for(const FramesHandle& fh : _frames_handler_vector) {
      if(fh.child_frame_id == child_frame_id) {
        sensor_name = fh.name;
        break;
      }
    }
    if(sensor_name.empty())
      return;

    Vector6 v;
    v << (*tf_message_).transforms[0].transform.translation.x,
      (*tf_message_).transforms[0].transform.translation.y,
      (*tf_message_).transforms[0].transform.translation.z,
      (*tf_message_).transforms[0].transform.rotation.x,
      (*tf_message_).transforms[0].transform.rotation.y,
      (*tf_message_).transforms[0].transform.rotation.z;    
    Isometry3 T = srrg_core::v2t(v);

    MeasurePtr measure(new Measure(T));
    sensor_dataset_.find(sensor_name)->second->insert(Sample(timestamp_, measure));
    
  }

  void BagLoader::generateImageMessage(SensorDatasetMap& sensor_dataset_,
                                       const sensor_msgs::ImageConstPtr& image_message_,
                                       const double& timestamp_) {

    MeasurePtr measure(new Measure(Isometry3::Identity()));

    cv_bridge::CvImageConstPtr depth_img;
   
    try {
      depth_img = cv_bridge::toCvCopy(image_message_, "");
    }
    catch (cv_bridge::Exception& e) {
      ROS_ERROR("Failed to transform depth image.");
      return;
    }
  
    FloatImage float_image;
    if(depth_img->image.type() == CV_16UC1)
      srrg_core::convert_16UC1_to_32FC1(float_image, depth_img->image, 0.001);
    else
    float_image = depth_img->image;

    measure->setDepthImage(float_image);
    Sample image_sample(timestamp_, measure);
    sensor_dataset_.find(_image_topics.first)->second->insert(image_sample);
  }
  
 
  void BagLoader::load(Dataset& dataset_,
                       SensorDatasetMap& sensor_dataset_) {
    
    rosbag::Bag bag;
    std::cerr << "[BagLoader][load]: opening Bag ...";
    bag.open(_bag_file, rosbag::bagmode::Read);
    std::cerr << "done" << std::endl;
    std::vector<std::string> topics;
    topics.push_back("/tf");
    if(!_joint_topic.empty())
      topics.push_back(_joint_topic);
    if(!_odom_topic.empty())
      topics.push_back(_odom_topic);
    if(!_image_topics.second.empty())
      topics.push_back(_image_topics.second);    
    
    
    rosbag::View view(bag, rosbag::TopicQuery(topics));

    ros::Time init_time;

    int num_images = 0;
    
    // iterate over the rosbag view
    for(rosbag::View::iterator itMessage = view.begin(); itMessage != view.end(); ++itMessage) {
      const ros::Time timestamp = itMessage->getTime();
      //bdc skip first message to set a time reference
      if(itMessage == view.begin()) {
        init_time = timestamp;
        continue;
      }
      const double current_timestamp = (timestamp// - init_time
                                        ).toSec();
      if(current_timestamp < 1482340309.0)
        continue;
      const std::string topic = itMessage->getTopic();

      if(topic == _joint_topic) {
        sensor_msgs::JointStateConstPtr joint_message = itMessage->instantiate<sensor_msgs::JointState>();
        generateJointMessage(dataset_, joint_message, current_timestamp);
      } else if(topic == _odom_topic) {
        nav_msgs::OdometryConstPtr odom_message = itMessage->instantiate<nav_msgs::Odometry>();
        generateOdomMessage(dataset_, odom_message, current_timestamp);
      } else if(topic == "/tf") {
        tf2_msgs::TFMessageConstPtr tf_message = itMessage->instantiate<tf2_msgs::TFMessage>();
        generateTfMessage(sensor_dataset_, tf_message, current_timestamp);
      } else if(topic == _image_topics.second){
        sensor_msgs::ImageConstPtr image_message = itMessage->instantiate<sensor_msgs::Image>();
        generateImageMessage(sensor_dataset_, image_message, current_timestamp);
        ++num_images;
      } else {
        std::cerr << FYEL("unkwnow message discarded with topic: ") << topic << std::endl;
      }             
    }

    std::cerr << FBLU("BagLoader recap") << std::endl;
    std::cerr << FBLU("  - reference msgs: ") << dataset_.size() << std::endl;
    for(const SensorDatasetPair& string_data : sensor_dataset_)
      std::cerr << KBLU << "  - " << string_data.first << " msgs:  " << RST << string_data.second->size() << std::endl;
    std::cerr << FBLU("----------------") << std::endl;
    
  }
    
   
}
