cmake_minimum_required(VERSION 2.8.3)
project(srrg_lifelong_calibration_ros)

#set the default path for built executables to the "bin" directory
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
#set the default path for built libraries to the "lib" directory
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

# To uncomment in the futute
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --std=gnu++11 -march=native")

find_package(srrg_cmake_modules REQUIRED)
set(CMAKE_MODULE_PATH ${srrg_cmake_modules_INCLUDE_DIRS})

set(CMAKE_BUILD_TYPE Release)

message(STATUS "PROCESSOR_TYPE: [${CMAKE_HOST_SYSTEM_PROCESSOR}]")
if (${CMAKE_HOST_SYSTEM_PROCESSOR} MATCHES armv7l)
  set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -march=armv7-a -mfpu=neon-vfpv4 -mfloat-abi=hard -funsafe-math-optimizations")
  set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=armv7-a -mfpu=neon-vfpv4 -mfloat-abi=hard -funsafe-math-optimizations")
  message("ENABLING ARM NEON OPTIMIZATIONS")
endif ()

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  srrg_cmake_modules
  srrg_core
  srrg_lifelong_calibration
  geometry_msgs
  nav_msgs
  roscpp
  rospy
  sensor_msgs
  std_msgs
  message_filters
  tf
  rosbag
  cv_bridge 
  )

find_package(Eigen3 REQUIRED)
include_directories(${EIGEN3_INCLUDE_DIR})

find_package(OpenCV REQUIRED)
message(STATUS "${PROJECT_NAME}: OpenCV version ${OpenCV_VERSION}")
include_directories(${OpenCV_INCLUDE_DIRS})
message(STATUS "OpenCV_INCLUDE_DIRS: ${OpenCV_INCLUDE_DIRS}")

# # Find OpenGL
# find_package(OpenGL REQUIRED)
# include_directories(${OPENGL_INCLUDE})

# # Find QGLViewer
# find_package(QGLViewer REQUIRED)
# include_directories(${QGLVIEWER_INCLUDE_DIR})
# message(STATUS "SRRG_QT_INCLUDE_DIRS: ${SRRG_QT_INCLUDE_DIRS}")
# include_directories(${SRRG_QT_INCLUDE_DIRS})


###################################
## catkin specific configuration ##
###################################
catkin_package(
  INCLUDE_DIRS ${PROJECT_SOURCE_DIR}/src
  LIBRARIES srrg_l2c_ros_library
  CATKIN_DEPENDS srrg_cmake_modules srrg_core srrg_lifelong_calibration
# DEPENDS system_lib
)


###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  ${PROJECT_SOURCE_DIR}/src
  ${catkin_INCLUDE_DIRS}
)

add_subdirectory(${PROJECT_SOURCE_DIR}/src)

